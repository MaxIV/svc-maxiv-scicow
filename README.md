# svc-maxiv-scicow

Monitors Kafka topic and makes the corresponding update in SciCat through the REST interface. Each beamline which wants to push data into scicat should have a topic in the kafka cluster.

## Deployment
For production deployment, the Kafka topic should always be the name of the beamline. For example production deployment for BioMAX should have topic name `biomax`. For test deployment, the Kafka topic should always be a combination of the name of the beamline and `-test`. For example test deployment for BioMAX should have topic name `biomax-test`. This is done automatically now with the kafka-user-topic-creator helm chart (https://gitlab.maxiv.lu.se/kits-maxiv/kubernetes/helm-maxiv-scicow/-/blob/main/kafka-user-topic-creator/values.yaml).

The test development is running in OKDev under the namespace `scicow` and pushes to SciCat test database.

The production deployment is running in OKD under the namespace `scicow` and pushes to the SciCat production database.

## How it works

One instance of SciCow is connected to all beamlines topics, one instance is running for the test topics and one for production. When a message is put in the queue, SciCow is triggered. It will read the message, connect to DUO to find the relevant proposal information and upload a `Dataset` to SciCat. Furthermore, if required, it will upload a `OrigDatablocks` to SciCat as well, linking the relevant data files on the storage to the dataset.
