mock_os_env = {
    "BOOTSTRAP_SERVERS": "mocked_bootstrap_servers",
    "BEAMLINES": '["valid_beamline"]',
    "SCICAT_PID_PREFIX": "test_prefix",
    "ENVIRONMENT": "testing",
    "SCICAT_URL": "https://example.maxiv.lu.se",
    "SCICAT_CREDS_USERNAME": "username",
    "SCICAT_CREDS_PWD": "pwd",
    "DUO_URL": "https://example.maxiv.lu.se",
    "DUO_CREDS_USERNAME": "username",
    "DUO_CREDS_PWD": "pwd",
}
