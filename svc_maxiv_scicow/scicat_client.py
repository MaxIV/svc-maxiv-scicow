from datetime import datetime, timedelta, timezone
import logging

from scicat_sdk_py import (
    ApiClient,
    AuthApi,
    DatasetsV4Api,
    OrigdatablocksApi,
    ProposalsApi,
    configuration,
)
from scicat_sdk_py.models import credentials_dto

from .config import Config

logger = logging.getLogger(__name__)


class ScicatClient:
    """
    A client for interacting with the SciCat API.

    Two API clients are needed: one for logging in and one that is using a valid token.
    The token is renewed if needed and the second API client recreated.

    ScicatClient handles authentication and provides access to various parts of the SciCat API,
    including datasets, original data blocks, and proposals.
    """

    def __init__(self, config: Config):
        self.env_config = config
        self.api_client: ApiClient | None = None
        self.token_expiry: datetime | None = None

        login_config = configuration.Configuration(
            host=config.SCICAT_CLIENT["base_url"]
        )
        api_client = ApiClient(configuration=login_config)
        self.auth_api = AuthApi(api_client)
        self.credentials = credentials_dto.CredentialsDto(
            username=config.SCICAT_CLIENT["username"],
            password=config.SCICAT_CLIENT["password"],
        )

    def create_api_client(self, token: str) -> ApiClient:
        config = configuration.Configuration(
            host=self.env_config.SCICAT_CLIENT["base_url"], access_token=token
        )
        return ApiClient(configuration=config)

    def get_api_client(self) -> ApiClient:
        if self.api_client is None or self.new_token_needed():
            logger.info("New token needed, logging in.")
            token = self.login()
            self.api_client = self.create_api_client(token)
        return self.api_client

    def get_datasets_api(self) -> DatasetsV4Api:
        api_client = self.get_api_client()
        return DatasetsV4Api(api_client)

    def get_origdatablocks_api(self) -> OrigdatablocksApi:
        api_client = self.get_api_client()
        return OrigdatablocksApi(api_client)

    def get_proposals_api(self) -> ProposalsApi:
        api_client = self.get_api_client()
        return ProposalsApi(api_client)

    def login(self) -> str:
        auth_dto = self.auth_api.auth_controller_login(self.credentials)
        self.token_expiry = datetime.fromisoformat(auth_dto.created) + timedelta(
            seconds=auth_dto.ttl
        )
        return auth_dto.access_token

    def new_token_needed(self) -> bool:
        if not self.token_expiry:
            return True
        elif datetime.now(timezone.utc) > self.token_expiry - timedelta(
            seconds=self.env_config.BUFFER_SECONDS
        ):
            return True
        else:
            return False
