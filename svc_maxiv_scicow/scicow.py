import logging
from datetime import UTC, datetime

from requests.exceptions import HTTPError
from scicat_sdk_py.exceptions import NotFoundException
from scicat_sdk_py.models.create_dataset_dto import CreateDatasetDto
from scicat_sdk_py.models.create_orig_datablock_dto import CreateOrigDatablockDto
from scicat_sdk_py.models.create_proposal_dto import CreateProposalDto
from scicat_sdk_py.models.data_file import DataFile
from scicat_sdk_py.models.partial_update_dataset_dto import PartialUpdateDatasetDto

from .config import Config
from .exceptions import DuoConnectionException, SciCatRequestError
from .scicat_client import ScicatClient
from .scicow_types import Ownable

logger = logging.getLogger(__name__)


class SciCow:
    def __init__(self, env_config: Config, duo_client, scicat_client: ScicatClient):
        self.env_config = env_config
        self.scicat = scicat_client
        self.duo = duo_client
        self.last_proposal_id = ""
        self.beamlines = {
            beamline.lower(): beamline for beamline in self.env_config.BEAMLINES
        }

    def process_data(self, data, topic):
        if "event" not in data:
            logger.warning(f"No event key found, ignorning. Message: {data}")
            return

        if topic not in self.beamlines:
            logger.error(f"Topic {topic} not found among beamlines. Unable to proceed.")
            return

        beamline = self.beamlines[topic]

        if "experiment-began" in data["event"]:
            self.process_began(data, beamline)
        elif "experiment-ended" in data["event"]:
            self.process_ended(data, beamline)
        else:
            logger.warning(f"Event key invalid, ignorning. Message: {data}")
            return

    def process_began(self, data: dict, beamline: str):
        # skip if the proposalId is not set, should be beamline testing
        if "proposalId" not in data or not data["proposalId"]:
            logger.info("Proposal id not set, skipping.")
            return

        try:
            # Get DUO information
            proposal, authors = self.duo.get_proposal_and_authors(data["proposalId"])
            principal_id = [
                author["userid"] for author in authors if author["relation"] == "I"
            ][0]
            principal = self.duo.get_client(principal_id)
        except Exception:
            logger.exception(
                f"Could not connect to duo for {data['proposalId']}, skipping"
            )
            raise DuoConnectionException("Could not connect to DUO API")

        try:
            persistant_identifier = self.env_config.SCICAT_PID_PREFIX + data["uuid"]
            ownable = Ownable(
                ownerGroup=f"{data['proposalId']}-group",
                accessGroups=[beamline.lower()],
                instrumentGroup=beamline.lower(),
            )
            create_dto = CreateDatasetDto(
                **ownable.dict(),
                pid=persistant_identifier,
                owner=f"{principal['firstname']} {principal['lastname']}",
                ownerEmail=principal["email"],
                contactEmail=principal["email"],
                sourceFolder=data["sourceFolder"],
                principalInvestigators=[principal["email"]],
                creationTime=datetime.fromtimestamp(data["creationTime"], UTC),
                type="raw",
                datasetName=data["datasetName"],
                description=data["description"],
                creationLocation=beamline,
                proposalIds=[data["proposalId"]],
                scientificMetadata=data["scientificMetadata"],
                dataFormat=data["dataFormat"],
                comment=data.get("comment"),
            )
        except Exception:
            logger.exception(
                "Unexpected Error in process_began. Stopping proccessing of data."
            )
            return

        try:
            self.scicat.get_datasets_api().datasets_v4_controller_create(create_dto)
        except Exception:
            logger.exception(
                f"Tried to upload for {persistant_identifier} to SciCat but failed"
            )
            raise SciCatRequestError

        logger.info(f"Uploaded dataset to SciCat with PID: {persistant_identifier}")

        # Datablock and Proposal processing
        try:
            if len(data["files"]) > 0:
                self.datablock_process(data["files"], persistant_identifier, ownable)
            if self.last_proposal_id != data["proposalId"]:
                self.proposal_process(
                    persistant_identifier,
                    data["proposalId"],
                    proposal,
                    principal,
                    ownable,
                )
                self.last_proposal_id = data["proposalId"]
        except Exception:
            logger.exception(
                "Skipping data files due to error in datablock and proposal processing."
            )
            return

    def process_ended(self, data: dict, beamline: str):
        pid = self.env_config.SCICAT_PID_PREFIX + data["uuid"]
        try:
            dataset = self.scicat.get_datasets_api().datasets_v4_controller_find_by_id(
                pid
            )
        except Exception:
            logger.warning(
                f"Dataset with PID: {pid} not ingested yet, so cannot update."
            )
            raise SciCatRequestError

        ownable = Ownable(
            ownerGroup=dataset.owner_group,
            accessGroups=[beamline.lower()],
            instrumentGroup=beamline.lower(),
        )
        try:
            if len(data["files"]) > 0:
                self.datablock_process(data["files"], pid, ownable)

            update_dto = PartialUpdateDatasetDto(
                scientificMetadata=data["scientificMetadata"]
            )
            self.scicat.get_datasets_api().datasets_v4_controller_find_by_id_and_update(
                pid, update_dto
            )
            logger.info(f"Patched dataset to SciCat with PID: {pid}")
        except HTTPError as e:
            if e.response.status_code == 404:
                logger.warning(
                    f"Skipping. Trying to process non-existing dataset {pid}"
                )
            else:
                logger.exception(f"Failed to process {pid}")
        except Exception:
            logger.exception(f"Tried to patch for {pid} to SciCat but failed")

    def datablock_process(self, files: list[dict], dataset_id: str, ownable: Ownable):
        try:
            data_block = CreateOrigDatablockDto(
                size=sum(file["size"] for file in files),
                datasetId=dataset_id,
                dataFileList=[DataFile(**file) for file in files],
                ownerGroup=ownable.ownerGroup,
                accessGroups=ownable.accessGroups,
                chkAlg="sha1",
            )
            self.scicat.get_origdatablocks_api().orig_datablocks_controller_create(
                data_block
            )
        except Exception:
            logger.exception(
                f"Tried to save files for {dataset_id} to SciCat but failed"
            )

    def proposal_process(self, pid, proposal_id, proposal, principal, ownable):
        try:
            self.scicat.get_proposals_api().proposals_controller_find_by_id(proposal_id)
            logger.info(f"Proposal {proposal_id} already exists, skipping")
            return
        except NotFoundException:
            pass
        except Exception:
            logger.exception(f"Error in proposal_process for propid = {proposal_id}.")
            return

        proposal_dates = None
        try:
            proposal_dates = self.duo.get_proposal_dates(proposal_id)
        except Exception:
            logger.exception(
                f"Tried to get dates for proposal {proposal_id} but failed."
            )

        try:
            logger.info(f"Update/save proposal information to SciCat {proposal_id}")
            create_proposal_dto = CreateProposalDto(
                proposalId=str(proposal["propid"]),
                pi_email=principal["email"],
                pi_firstname=principal["firstname"],
                pi_lastname=principal["lastname"],
                email=principal["email"],
                firstname=principal["firstname"],
                lastname=principal["lastname"],
                title=proposal["title"],
                abstract=proposal["abstract"],
                startTime=proposal_dates["firstshift"] if proposal_dates else None,
                endTime=proposal_dates["lastshift"] if proposal_dates else None,
                **ownable.dict(),
            )
            self.scicat.get_proposals_api().proposals_controller_create(
                create_proposal_dto
            )
        except Exception:
            logger.exception(
                f"Tried to save proposal {proposal_id} to SciCat but failed"
            )
