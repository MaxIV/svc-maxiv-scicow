import json
import os

from dotenv import load_dotenv


class Config:
    def __init__(self):
        # Get new token if existing token expires within BUFFER_SECONDS
        self.BUFFER_SECONDS = 60

        load_dotenv()
        self.BOOTSTRAP_SERVERS = os.environ["BOOTSTRAP_SERVERS"]
        self.BEAMLINES = json.loads(os.environ["BEAMLINES"])
        self.SCICAT_PID_PREFIX = os.environ["SCICAT_PID_PREFIX"]
        self.ENVIRONMENT = os.environ["ENVIRONMENT"]

        self.SCICAT_CLIENT = {
            "base_url": os.environ["SCICAT_URL"],
            "username": os.environ["SCICAT_CREDS_USERNAME"],
            "password": os.environ["SCICAT_CREDS_PWD"],
        }

        self.DUO_CLIENT = {
            "base_url": os.environ["DUO_URL"],
            "email": os.environ["DUO_CREDS_USERNAME"],
            "password": os.environ["DUO_CREDS_PWD"],
        }
