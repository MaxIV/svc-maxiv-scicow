import logging
from enum import StrEnum

import requests

from .config import Config

logger = logging.getLogger(__name__)


class HttpMethod(StrEnum):
    GET = "GET"
    POST = "POST"


class DuoClient:
    def __init__(self, config: Config) -> None:
        self._base_url = config.DUO_CLIENT["base_url"]
        self._email = config.DUO_CLIENT["email"]
        self._password = config.DUO_CLIENT["password"]
        self.retrieve_access_token()

    def get_client(self, client_id):
        url = f"{self._base_url}Clients/{client_id}"
        return self.duo_request_json(url, HttpMethod.GET)

    def get_proposal_and_authors(self, proposal_id):
        proposals_url = f"{self._base_url}Proposals/{proposal_id}"
        proposal = self.duo_request_json(proposals_url, HttpMethod.GET)

        authors_url = f"{self._base_url}Proposals/{proposal_id}/authors"
        authors = self.duo_request_json(authors_url, HttpMethod.GET)

        return proposal, authors

    def get_proposal_dates(self, proposal_id):
        url = f"{self._base_url}ProposalDates/{proposal_id}"
        return self.duo_request_json(url, HttpMethod.GET)

    def retrieve_access_token(self):
        res = requests.post(
            f"{self._base_url}users/login",
            data={"email": self._email, "password": self._password},
        )
        res.raise_for_status()
        token = res.json()["id"]
        self._headers = {"Authorization": token}
        logger.info(f"Logged in to DUO with token: {token}")

    def duo_request_json(self, url: str, method: HttpMethod):
        """Send request to DUO API, refresh token if needed"""
        response = requests.request(method, url, headers=self._headers)

        if response.status_code == 401:
            self.retrieve_access_token()
            response = requests.request(method, url, headers=self._headers)

        response.raise_for_status()
        return response.json()
