from unittest.mock import MagicMock, patch
from datetime import datetime

import pytest
from scicat_sdk_py.models.output_dataset_dto import OutputDatasetDto

from .exceptions import DuoConnectionException, SciCatRequestError
from .scicow import SciCow


def data_experiment_began_factory(proposal_id=""):
    return {
        "uuid": "/12345",
        "event": {"experiment-began": "value"},
        "proposalId": proposal_id,
        "files": [{"path": "/source/file.txt", "size": 42, "time": "2024-01-01"}],
        "sourceFolder": "theFolder",
        "datasetName": "theDataSetName",
        "description": "FancyDescription",
        "dataFormat": "lsr",
        "creationTime": 111111111,
        "comment": "Fancy Comment",
        "scientificMetadata": {"some": "data"},
    }


def client_factory():
    return {
        "firstname": "Someone",
        "lastname": "SuperName",
        "email": "the_owner_of@maxiv.lu.se",
    }


class MockConfig:
    BOOTSTRAP_SERVERS = "mocked_bootstrap_servers"
    BEAMLINES = ["valid_beamline"]
    SCICAT_PID_PREFIX = "test_prefix"
    ENVIRONMENT = "testing"
    SCICAT_URL = "https://example.maxiv.lu.se"
    SCICAT_CREDS_USERNAME = "username"
    SCICAT_CREDS_PWD = "pwd"
    DUO_URL = "https://example.maxiv.lu.se"
    DUO_CREDS_USERNAME = "username"
    DUO_CREDS_PWD = "pwd"


def data_experiment_ended_factory():
    return {"event": {"experiment-ended": "value"}}


@pytest.fixture
def mock_scicow():
    duo_client = MagicMock()
    scicat_client = MagicMock()
    env_config = MockConfig()
    scicow = SciCow(env_config, duo_client, scicat_client)

    scicow.process_began = MagicMock()
    scicow.process_ended = MagicMock()
    yield scicow


def test_process_data_no_event_in_data(mock_scicow):
    scicow = mock_scicow

    with patch("svc_maxiv_scicow.scicow.logger") as logger:
        data = {}
        scicow.process_data(data, "valid_beamline")

        logger.warning.assert_called_with(
            f"No event key found, ignorning. Message: {data}"
        )


def test_process_data_call_process_began(mock_scicow):
    scicow = mock_scicow
    data = data_experiment_began_factory()
    scicow.process_data(data, "valid_beamline")

    scicow.process_began.assert_called()
    scicow.process_ended.assert_not_called()


def test_process_data_call_process_ended(mock_scicow):
    scicow = mock_scicow

    data = data_experiment_ended_factory()
    scicow.process_data(data, "valid_beamline")

    scicow.process_began.assert_not_called()
    scicow.process_ended.assert_called()


def test_process_began_stops_with_no_proposal_set():
    duo_client = MagicMock()
    scicat_client = MagicMock()
    env_config = MockConfig()
    scicow = SciCow(env_config, duo_client, scicat_client)

    data = data_experiment_began_factory(proposal_id="")
    with patch("svc_maxiv_scicow.scicow.logger") as logger:
        scicow.process_data(data, "valid_beamline")
        logger.info.assert_called_with("Proposal id not set, skipping.")

    duo_client.get_proposal_and_authors.assert_not_called()


def test_process_began_raise_duo_exception_if_duo_fails():
    duo_client = MagicMock()
    scicat_client = MagicMock()
    env_config = MockConfig()
    scicow = SciCow(env_config, duo_client, scicat_client)

    duo_client.get_proposal_and_authors.side_effect = Exception()

    data = data_experiment_began_factory(proposal_id=20100001)
    with pytest.raises(DuoConnectionException):
        scicow.process_data(data, "valid_beamline")


def test_process_began_happy_path():
    duo_client = MagicMock()
    duo_client.get_proposal_and_authors.return_value = (
        "proposer",
        [{"userid": 12345, "relation": "I"}],
    )
    duo_client.get_client.return_value = client_factory()

    scicat_client = MagicMock()
    env_config = MockConfig()
    scicow = SciCow(env_config, duo_client, scicat_client)

    data = data_experiment_began_factory(proposal_id="20100001")
    scicow.process_data(data, "valid_beamline")

    duo_client.get_proposal_and_authors.assert_called()
    scicat_client.get_datasets_api.assert_called()


def test_process_began_exception_if_datasets_create_fail():
    duo_client = MagicMock()
    duo_client.get_proposal_and_authors.return_value = (
        "proposer",
        [{"userid": 12345, "relation": "I"}],
    )
    duo_client.get_client.return_value = client_factory()

    scicat_client = MagicMock()
    env_config = MockConfig()
    scicow = SciCow(env_config, duo_client, scicat_client)

    scicat_client.get_datasets_api().side_effect = Exception()

    scicat_client.get_datasets_api().datasets_v4_controller_create.side_effect = (
        Exception()
    )
    data = data_experiment_began_factory(proposal_id="20100001")

    with pytest.raises(SciCatRequestError):
        scicow.process_data(data, "valid_beamline")


def test_process_ended_exception_if_dataset_get_one_fails():
    duo_client = MagicMock()
    duo_client.get_proposal_and_authors.return_value = (
        "proposer",
        [{"userid": 12345, "relation": "I"}],
    )
    duo_client.get_client.return_value = client_factory()

    scicat_client = MagicMock()
    env_config = MockConfig()
    scicow = SciCow(env_config, duo_client, scicat_client)

    scicat_client.get_datasets_api().datasets_v4_controller_find_by_id.side_effect = (
        Exception()
    )

    with pytest.raises(SciCatRequestError):
        scicow.process_ended({"uuid": "uuid"}, "valid_beamline")


def test_process_ended_happy_path():
    duo_client = MagicMock()
    duo_client.get_proposal_and_authors.return_value = (
        "proposer",
        [{"userid": 12345, "relation": "I"}],
    )
    duo_client.get_client.return_value = client_factory()

    scicat_client = MagicMock()
    env_config = MockConfig()
    scicow = SciCow(env_config, duo_client, scicat_client)

    output_dataset_dto = OutputDatasetDto(
        ownerGroup="testOwnerGroup",
        contactEmail="contact@email.com",
        sourceFolder="/some/folder",
        creationTime=datetime.now(),
        datasetName="the datasetname",
        type="raw",
        createdBy="Emil",
        createdAt=datetime.now(),
        updatedAt=datetime.now(),
        updatedBy="Someone",
        version="42.0",
    )
    scicat_client.get_datasets_api().datasets_v4_controller_find_by_id.return_value = (
        output_dataset_dto
    )
    scicow.datablock_process = MagicMock()

    data = {
        "uuid": "uuid",
        "files": ["a_file"],
        "scientificMetadata": {},
    }

    scicow.process_ended(data, "valid_beamline")

    scicat_client.get_datasets_api().datasets_v4_controller_find_by_id.assert_called()
    scicow.datablock_process.assert_called()
    scicat_client.get_datasets_api().datasets_v4_controller_find_by_id_and_update.assert_called()
