import threading
import time
from unittest.mock import MagicMock, patch

import pytest

from test_common import mock_os_env

from .exceptions import DuoConnectionException


class DummyConsumer:
    def __init__(self):
        self.has_error = False
        self.message = '{"a": "message"}'
        self.kafka_topic = "my_topic"

    def error(self):
        return self.has_error

    def value(self):
        return self.message.encode(encoding="utf-8")

    def topic(self):
        return self.kafka_topic


def execute_run(runner):
    def run_in_thread():
        runner.run()

    runner_thread = threading.Thread(target=run_in_thread)
    runner_thread.start()
    time.sleep(0.01)
    runner.keep_running = False
    runner_thread.join()


@pytest.fixture
def mock_env(monkeypatch):
    for name, value in mock_os_env.items():
        monkeypatch.setenv(name, value)


@pytest.fixture
def mock_consumer(mock_env):
    with patch("svc_maxiv_scicow.runner.Consumer", autospec=True) as consumer:
        consumer.return_value = consumer
        consumer.poll.return_value = DummyConsumer()
        yield consumer


@pytest.fixture
def mock_scicow():
    with patch("svc_maxiv_scicow.runner.SciCow", autospec=True) as scicow:
        yield scicow


@pytest.fixture
def mock_duo_client():
    with patch("svc_maxiv_scicow.runner.DuoClient", autospec=True) as duo_client:
        yield duo_client


@pytest.fixture
def runner_fixture(mock_consumer, mock_duo_client):
    from .runner import Runner, argparse, signal

    argparse.ArgumentParser = MagicMock()
    signal.signal = MagicMock()

    runner = Runner()

    return runner


def test_basic_happy_consuming_path(runner_fixture, mock_scicow, mock_consumer):
    runner = runner_fixture
    runner.scicow = mock_scicow
    execute_run(runner)

    mock_consumer.poll.assert_called_with(timeout=1.0)
    runner.scicow.process_data.assert_called()
    mock_consumer.commit.assert_called()


def test_do_not_call_process_data_if_no_msg(runner_fixture, mock_scicow, mock_consumer):
    runner = runner_fixture
    mock_consumer.poll.return_value = None
    runner.process_failed_queue = MagicMock()
    execute_run(runner)

    mock_consumer.poll.assert_called_with(timeout=1.0)
    runner.process_failed_queue.assert_called()
    mock_scicow.process_data.assert_not_called()
    mock_consumer.commit.assert_not_called()


def test_faulty_json_does_commit(runner_fixture, mock_scicow, mock_consumer):
    runner = runner_fixture
    mock_consumer.poll.return_value.message = "{faulty json"
    execute_run(runner)

    mock_consumer.commit.assert_called()


def test_message_inserted_if_scicow_raises_duoexception(
    runner_fixture, mock_scicow, mock_consumer
):
    runner = runner_fixture
    runner.logger = MagicMock()
    runner.scicow = mock_scicow
    mock_scicow.process_data.side_effect = DuoConnectionException()

    runner.consume(mock_consumer)

    assert len(runner.failed_queue) > 0
    assert (
        runner.failed_queue[0][1].topic == "my_topic"
    )  # == mock_consumer.poll().kafka_topic
    assert runner.failed_queue[0][1].retries == 0
    assert runner.failed_queue[0][1].data["a"] == "message"
    mock_consumer.commit.assert_called()


def test_failed_messages_prioritized_by_timestamp(
    runner_fixture, mock_scicow, mock_consumer
):
    runner = runner_fixture
    with patch("svc_maxiv_scicow.runner.time", autospec=True) as time:
        time.time.return_value = 100
        runner.insert_failed_message({"my": "message1"}, "topic1")
        assert runner.failed_queue[0][0] == 100 + 60

        # assert that a failed_message with earlier timestamp moves to first in queue
        time.time.return_value = 50
        runner.insert_failed_message({"my": "message2"}, "topic2")
        assert runner.failed_queue[0][0] == 50 + 60


def test_get_next_try_add_minutes(runner_fixture, mock_scicow, mock_consumer):
    runner = runner_fixture
    from .runner import FailedMessage

    retries = 10
    failed_message = FailedMessage(dict(), "topic", 100, retries)

    add_minutes = 2**retries
    assert runner.get_next_try(failed_message) == 100 + 60 * add_minutes


def test_get_next_max_retries(runner_fixture, mock_scicow, mock_consumer):
    runner = runner_fixture
    from .runner import MAX_RETRIES, FailedMessage

    failed_message = FailedMessage(dict(), "topic", 100, MAX_RETRIES)

    assert runner.get_next_try(failed_message) is None


def test_process_failed_queue_calls_process_data(
    runner_fixture, mock_scicow, mock_consumer
):
    runner = runner_fixture
    from .runner import FailedMessage

    runner.scicow = mock_scicow
    failed_message = FailedMessage(dict(), "topic", 50, 5)
    runner.failed_queue.append((75, failed_message))
    with patch("svc_maxiv_scicow.runner.time", autospec=True) as time:
        time.time.return_value = 100
        runner.process_failed_queue()

    runner.scicow.process_data.assert_called()


def test_process_failed_queue_inserts_into_queue_after_fail(
    runner_fixture, mock_scicow, mock_consumer
):
    runner = runner_fixture
    from .runner import FailedMessage

    runner.scicow = mock_scicow
    runner.logger = runner._get_logger("INFO")
    mock_scicow.process_data.side_effect = DuoConnectionException()
    failed_message = FailedMessage(dict(), "topic", 50, 5)
    runner.failed_queue.append((75, failed_message))
    with patch("svc_maxiv_scicow.runner.time", autospec=True) as time:
        time.time.return_value = 100
        runner.process_failed_queue()

    # message has been inserted again due to DUO error
    assert len(runner.failed_queue) == 1


def test_process_failed_queue_skips_when_max_retries_reached(
    runner_fixture, mock_scicow, mock_consumer
):
    runner = runner_fixture
    from .runner import FailedMessage

    runner.scicow = mock_scicow
    runner.logger = MagicMock()
    mock_scicow.process_data.side_effect = DuoConnectionException()
    failed_message = FailedMessage(dict(), "topic", 50, 14)
    runner.failed_queue.append((75, failed_message))
    with patch("svc_maxiv_scicow.runner.time", autospec=True) as time:
        time.time.return_value = 100
        runner.process_failed_queue()

    # No message in queue since maximum tries has been reached
    assert len(runner.failed_queue) == 0
    # An error has been logged
    assert len(runner.logger.error.call_args_list) == 1


def test_log_at_shutdown(runner_fixture, mock_scicow, mock_consumer):
    runner = runner_fixture
    from .runner import FailedMessage

    assert runner.keep_running is True

    failed_message = FailedMessage({"test_key": "test_value"}, "test_topic", 50, 14)
    runner.failed_queue.append((75, failed_message))

    runner.logger = MagicMock()
    runner.install_signal_handler()
    runner.sigterm_handler("arg1", "arg2")

    runner.logger.info.assert_called_with(
        "Message in failed queue for test_topic thrown away at shutdown: {'test_key': 'test_value'}"
    )

    assert runner.keep_running is False
