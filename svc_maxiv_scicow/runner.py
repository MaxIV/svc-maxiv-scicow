import argparse
import json
import logging
import signal
import time
from dataclasses import dataclass
from heapq import heappop, heappush

from confluent_kafka import Consumer

from .scicow import SciCow
from .config import Config
from .duo import DuoClient
from .exceptions import DuoConnectionException, SciCatRequestError
from .scicat_client import ScicatClient

# Last try will occur at 2**(MAX_RETRIES-1) minutes after first poll. 14 = about six days
MAX_RETRIES = 14


@dataclass
class FailedMessage:
    """Class for tracking failed attempts to ingest a msg polled from Kafka"""

    data: object
    topic: str
    timestamp: float
    retries: int


class StopProgram(Exception):
    pass


class Runner:
    def __init__(self) -> None:
        self.failed_queue: list[tuple[float, FailedMessage]] = []
        self.keep_running = True

    def install_signal_handler(self):
        def sigterm_handler(signo, frame):
            for queue_item in self.failed_queue:
                failed_message = queue_item[1]
                self.logger.info(
                    f"Message in failed queue for {failed_message.topic} thrown away at shutdown: {failed_message.data}"
                )
            self.keep_running = False

        self.sigterm_handler = sigterm_handler
        signal.signal(signal.SIGTERM, sigterm_handler)

    def start_consuming(self, consumer):
        while self.keep_running:
            self.consume(consumer)

    def consume(self, consumer):
        msg = consumer.poll(timeout=1.0)
        if msg is None:
            # check if there are failed messages in queue and process one
            try:
                self.process_failed_queue()
            except Exception:
                self.logger.exception(
                    "Unexpected exception when processing failed messages"
                )
            return
        if msg.error():
            self.logger.info("Message error {}".format(msg.error()))
            return
        try:
            body = msg.value().decode("utf-8")
            data = json.loads(body)
        except Exception:
            self.logger.exception("Decode error while handling message.")
            # commit since this problem is permanent for this message
            consumer.commit()
            return

        try:
            self.scicow.process_data(data, msg.topic())
        except (DuoConnectionException, SciCatRequestError):
            self.insert_failed_message(data, msg.topic())
        except Exception:
            # if this happens it should be investigated
            # allow to commit since it might happen every time the message
            # and metadata could have been ingested
            self.logger.exception("Unknown error when handling message.")

        try:
            consumer.commit()
        except Exception:
            self.logger.exception(f"Failed to commit to Kafka with data: {data}")

    def insert_failed_message(self, data: object, topic: str) -> None:
        failed_message = FailedMessage(data, topic, time.time(), 0)
        next_try = self.get_next_try(failed_message)
        heappush(self.failed_queue, (next_try, failed_message))  #  type: ignore[misc]

    def get_next_try(self, failed_message: FailedMessage) -> float | None:
        if failed_message.retries >= MAX_RETRIES:
            return None

        add_minutes = 2**failed_message.retries
        return failed_message.timestamp + 60 * add_minutes

    def process_failed_queue(self):
        # check if it is time to ingest a previously failed message
        if len(self.failed_queue) == 0 or time.time() < self.failed_queue[0][0]:
            return

        message = heappop(self.failed_queue)[1]

        try:
            self.scicow.process_data(message.data, message.topic)
            return
        except (DuoConnectionException, SciCatRequestError) as e:
            self.logger.warning(f"Unable to process data due to {e.__class__.__name__}")
        except Exception:
            self.logger.exception(
                "Unexpected error while processing data in failed queue."
            )

        # insert into failed_queue again
        self.logger.info(
            f"Inserting {message.data} for topic {message.topic} into queue."
        )
        message.retries += 1
        next_try = self.get_next_try(message)
        if next_try:
            heappush(self.failed_queue, (next_try, message))
        else:
            self.logger.error(
                f"Maximum failed tries reached. NOT inserting {message.data} for topic {message.topic}"
            )

    def run(self):
        # Main
        parser = argparse.ArgumentParser(
            prog="scicow",
            description=(
                "SciCow - Scientific Consumer Of Wisdom. Kafka Consumer for SciCat"
            ),
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )

        parser.add_argument(
            "--verbose",
            dest="verbose",
            action="store_true",
            help="Set verbose output",
        )

        args = parser.parse_args()

        log_level = "INFO"
        if args.verbose:
            log_level = "DEBUG"

        self.logger = self._get_logger(log_level)
        self.install_signal_handler()

        # Start application
        env_config = Config()
        config = {
            "bootstrap.servers": env_config.BOOTSTRAP_SERVERS,
            "group.id": "scicow-" + env_config.ENVIRONMENT,
            "session.timeout.ms": 6000,
            "auto.offset.reset": "earliest",
            "security.protocol": "SSL",
            "ssl.ca.location": "/etc/ssl/certs/kafka/ca.crt",
            "ssl.key.location": "/etc/ssl/certs/kafka/user.key",
            "ssl.certificate.location": "/etc/ssl/certs/kafka/user.crt",
        }
        consumer = Consumer(config)
        scicat_client = ScicatClient(env_config)
        duo_client = DuoClient(env_config)
        self.scicow = SciCow(env_config, duo_client, scicat_client)

        topics = [topic.lower() for topic in env_config.BEAMLINES]

        self.logger.info("Starting consumer")
        self.logger.info(f'Listening to {",".join(topics)}')

        try:
            consumer.subscribe(topics)
            self.start_consuming(consumer)

        except (StopProgram, KeyboardInterrupt):
            self.logger.info("Stopping program")
            consumer.close()

        return 0

    def _get_logger(self, log_level):
        # Make handler which writes the logs to console.
        handler = logging.StreamHandler()

        # Let root logger use the file handler.
        root_logger = logging.getLogger()
        root_logger.addHandler(handler)

        # Log level for all modules.
        root_logger.setLevel(log_level)

        return root_logger
