from dataclasses import asdict, dataclass


@dataclass
class Ownable:
    ownerGroup: str
    accessGroups: list[str]
    instrumentGroup: str

    def dict(self):
        return asdict(self)
