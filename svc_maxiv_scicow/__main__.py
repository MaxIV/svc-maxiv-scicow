import sys

from .runner import Runner


def main():
    return Runner().run()


if __name__ == "__main__":
    sys.exit(main())
