class DuoConnectionException(Exception):
    """Failed to connect to DUO API"""


class SciCatRequestError(Exception):
    """Request to SciCat failed before insert"""
